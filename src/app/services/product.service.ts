import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product';
import { Observable } from 'rxjs';
import { IProduct } from '../interfaces/product.interface';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  public path: string;

  constructor(private _http: HttpClient) {
    this.path = '/products';
   }


   public getProducts(id: string): Observable<Product[]> {

    let url = `${environment.api.url}${this.path}`;
    const products: Product[] = [];

    if (id) {
      url += `?category_id=${id}`;
    }

    return this._http.get<IProduct[]>(url).pipe(
      map ( a => {
        for (const e of a) {
          products.push(new Product(e.id, e.name, e.description, e.image, e.price, e.category_id));
        }
        return products;
      } )
    )


   }
}
