import { Injectable } from '@angular/core';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  public cartedProducts: Product[];

  constructor() {

    this.cartedProducts = [];
   }


  public addProduct(product: Product): Product[] {

    this.cartedProducts.push(new Product(product.id, product.name, product.description, product.image, product.price, product.category_id));
    return this.cartedProducts;

  }
}
