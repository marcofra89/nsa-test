import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Category } from '../models/category';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICategory } from '../interfaces/category.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  public categories: Category[];
  public path: string;

  constructor(private _http: HttpClient) {

    this.path = '/categories';
    this.categories = [];
   }

   public getCategories(): Observable<Category[]> {

    return this._http.get<ICategory[]>(`${environment.api.url}${this.path}`).pipe(
      map ( a => {
        for (const e of a ) {
          this.categories.push(new Category(e.id, e.name));
        }
        return this.categories;
      })
    );

   }





}
