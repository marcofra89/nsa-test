export class Product {
    constructor(
        public readonly id: number,
        public name: string,
        public description: string,
        public image: string,
        public price: number,
        public category_id: number,
    ) {

    }
}
