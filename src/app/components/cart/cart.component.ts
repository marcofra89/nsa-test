import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, OnChanges {

  @Input() chartItem: Product;

  constructor(private _cartService: CartService) { }

  ngOnInit() {
  }

  ngOnChanges() {


  }


}
