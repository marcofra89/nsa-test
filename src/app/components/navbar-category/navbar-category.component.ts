import { Component, OnInit, Input } from '@angular/core';
import { Category } from 'src/app/models/category';

@Component({
  selector: '[app-navbar-category]',
  templateUrl: './navbar-category.component.html',
  styleUrls: ['./navbar-category.component.scss']
})
export class NavbarCategoryComponent implements OnInit {

  @Input() navbarCategoy: Category;

  constructor() { }

  ngOnInit() {
  }

}
