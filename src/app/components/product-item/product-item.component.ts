import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/models/product';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: '[app-product-item]',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {

  @Input() productItem: Product;

  constructor(private _cartService: CartService) { }

  ngOnInit() {
  }


  public sendToChart( product: Product ) {

    this._cartService.addProduct(product);


  }

}
