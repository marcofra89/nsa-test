import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public categories: Category[];

  constructor(private _categoryService: CategoryService) { }

  ngOnInit() {

    this._categoryService.getCategories().subscribe(
      a => {
        this.categories = a;
      }
    )

  }

}
