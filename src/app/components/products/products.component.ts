import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  public products: Product[];

  constructor(private _productService: ProductService, private _router: ActivatedRoute) {
    this.products = [];

   }

  ngOnInit() {

    this._router.paramMap.subscribe(
      a => {
        this._productService.getProducts(a.get('id')).subscribe(
          e => {
            this.products = e;
          }
        )
      }
    )




  }

}
