import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './components/products/products.component';
import { CartComponent } from './components/cart/cart.component';

const routes: Routes = [{
  component: ProductsComponent,
  path: '',
}, {
  component: ProductsComponent,
  path: ':id'
}, {
  component: CartComponent,
  path: 'cart/1'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
